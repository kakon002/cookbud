﻿// added new needs to change

using System;
using System.Diagnostics;
using System.IO;
using Cookbud.DependencyServices;
using Cookbud.Droid.DependencyServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileAccessServiceAndroid))]

namespace Cookbud.Droid.DependencyServices
{
    public class FileAccessServiceAndroid: IFileAccessService
    {
     

        public string GetSqLiteDatabasePath(string databaseName)
        {
            string personalFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            var dbPath = Path.Combine(personalFolderPath, databaseName);
            Console.WriteLine($"*** {this.GetType().Name}.{nameof(GetSqLiteDatabasePath)}: returning:[{dbPath}]");

            return dbPath;

        }
    }
}
