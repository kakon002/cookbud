﻿using System.Diagnostics;
using Prism;
using Prism.Ioc;
using Prism.Unity;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Cookbud.ViewModel;
using Cookbud.Views;
using Cookbud.Services;


[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Cookbud
{
    public partial class App : PrismApplication
    {
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnInitialized)}");
            InitializeComponent();

            await NavigationService.NavigateAsync($"{nameof(NavigationPage)}/{nameof(CookbudPage)}");
        }

        //register views + VM
        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(RegisterTypes)}");
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<CookbudPage, CookBudPageVM>();
            containerRegistry.RegisterForNavigation<RecipesPage, RecipesPageVM>();
            containerRegistry.RegisterForNavigation<addRecipePage,addRecipePageVM>();
            containerRegistry.RegisterForNavigation<aboutPage, aboutPageVM>();
            containerRegistry.RegisterSingleton<IRepository, Repository>();

        }

        protected override void OnStart()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnStart)}");
            base.OnStart();
        }

        protected override void OnSleep()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnSleep)}");
            base.OnSleep();
        }

        protected override void OnResume()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnResume)}");
            base.OnResume();
        }
    }
}
