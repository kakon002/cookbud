﻿using System;
using Prism.Mvvm;
using SQLite;

namespace Cookbud.Models
{
    [Table("recipetopic")]
    public class Recipe : BindableBase
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [MaxLength(30)]
        public string RecipeName { get; set; }

        [MaxLength(500)]
        public string webUrl { get; set; }

        [MaxLength(2500)]
        public string Details { get; set; }

        public override string ToString()
        {
            return $"RecipeName = {RecipeName}, webUrl:{webUrl}, Details:{Details}";
        }
    
    }
}
