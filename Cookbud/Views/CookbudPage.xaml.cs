﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Cookbud.Views
{
    public partial class CookbudPage : ContentPage
    {
        public CookbudPage()
        {
            Debug.WriteLine($"*** {this.GetType().Name}.{nameof(CookbudPage)} Constructor");
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
    }
}
