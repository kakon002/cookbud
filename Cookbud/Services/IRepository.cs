﻿using System;
using System.Threading.Tasks;
using Cookbud.Models;
using System.Collections.Generic;
using Cookbud.DependencyServices;
using System.Collections.ObjectModel;

namespace Cookbud.Services
{
    public interface IRepository
    {
        Task<int> SaveRecipeTopic(Recipe recipeTopicToSave);
        Task<ObservableCollection<Recipe>> GetRecipeTopics();
        void DeleteRecipeTopic(Recipe recipeTopicToDelete);

    }
}
