using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Cookbud.Models;
using Cookbud.DependencyServices;
using SQLite;
using System.Diagnostics;

namespace Cookbud.Services
{
    public class Repository: IRepository
    {
        private IFileAccessService _fileAccessService;
        private SQLiteAsyncConnection _sqliteConnection;
  
       
        public Repository(IFileAccessService fileAccessService)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(Repository)}:  ctor");

            _fileAccessService = fileAccessService;

            var databaseFilePath = _fileAccessService.GetSqLiteDatabasePath("recipelist.db3");
            _sqliteConnection = new SQLiteAsyncConnection(databaseFilePath);
            CreateTablesSynchronously();
        }

        private void CreateTablesSynchronously()
        {
            _sqliteConnection.CreateTableAsync<Recipe>().Wait();
        }

        public async void DeleteRecipeTopic(Recipe recipeTopicToDelete)
        {
            await _sqliteConnection.DeleteAsync(recipeTopicToDelete);
        }

        public async Task<int> SaveRecipeTopic(Recipe recipeToSave)
        {
            try{
                if(recipeToSave.Id ==0)
                {
                    recipeToSave.Id = await _sqliteConnection.InsertAsync(recipeToSave);
                }

                else
                {
                    await _sqliteConnection.UpdateAsync(recipeToSave);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(SaveRecipeTopic)}:  Exception {ex}");
            }
            return recipeToSave.Id;
        }

     

        public async Task<ObservableCollection<Recipe>> GetRecipeTopics()
        {
            var allTopics = new ObservableCollection<Recipe>();
            try{
                List<Recipe> topicList = await _sqliteConnection.Table<Recipe>().ToListAsync();
                allTopics = new ObservableCollection<Recipe>(topicList);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"**** {this.GetType().Name}.{nameof(GetRecipeTopics)}:  Exception {ex}");
            }
            return allTopics;
        }

    }
}

