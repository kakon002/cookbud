﻿using System;

namespace Cookbud.DependencyServices
{
    public interface IFileAccessService
    {
        string GetSqLiteDatabasePath(string databaseName);
    }
}
