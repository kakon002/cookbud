﻿using System;
using System.Diagnostics;
using Prism.Navigation;
using Xamarin.Forms;

namespace Cookbud.ViewModel
{
    public class aboutPageVM : ViewModelBase
    {
        public aboutPageVM(INavigationService navigationService) : base(navigationService)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(addRecipePageVM)}:  constructor");
            Title = "Help with Cookbud";
        }
    }
}

