﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Cookbud.Models;
using Cookbud.Services;
using System.Diagnostics;
using Cookbud.Views;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.ComponentModel;
using Xamarin.Forms;
using Prism.Services;

namespace Cookbud.ViewModel
{
    public class RecipesPageVM : ViewModelBase
    {
        private IRepository _repository;
        IPageDialogService _pageDialogService;


        //Commands
        public DelegateCommand AddRecipeCommand { get; set; }
        public DelegateCommand PullToRefreshCommand { get; set; }
        public DelegateCommand<Recipe> RecipeTappedCommand { get; set; }
        public DelegateCommand<Recipe> RecipeSelectedCommand { get; set; }
        public DelegateCommand<Recipe> DeleteCommand { get; set; }
        public DelegateCommand<Recipe> EditCommand { get; set; }
        public DelegateCommand searchCommand { get; set; }

        private string _bar;
        public string Bar
        {
            get { return _bar; } set { SetProperty(ref _bar, value); }
        }

        private ObservableCollection<Recipe> _recipe;
        public ObservableCollection<Recipe> Recipes
        {
            get { return _recipe; } set { SetProperty(ref _recipe, value); }
        }
      
        private bool _showIsBusySpinner;
        public bool ShowIsBusySpinner
        {
            get { return _showIsBusySpinner; }set { SetProperty(ref _showIsBusySpinner, value); }
        }

        private Recipe _selectedRecipe;
        public Recipe SelectedRecipe
        {
            get { return _selectedRecipe; }set { SetProperty(ref _selectedRecipe, value); }
        }


        public RecipesPageVM(INavigationService navigationService, IRepository repository, IPageDialogService pageDialogService): base(navigationService)
        {
            Debug.WriteLine($"*** {this.GetType().Name}.{nameof(RecipesPageVM)}: constructor");
            _repository = repository;
            _pageDialogService = pageDialogService;

            Title = "Recipes List";//xmal page title 

            AddRecipeCommand = new DelegateCommand(OnNavToAdd);//Navigates to add recipe page
            PullToRefreshCommand = new DelegateCommand(OnPullToRefresh);
            RecipeTappedCommand = new DelegateCommand<Recipe>(OnRecipeTapped);
            RecipeSelectedCommand = new DelegateCommand<Recipe>(OnRecipeSelected);
            DeleteCommand = new DelegateCommand<Recipe>(OnDeleteTapped);
            EditCommand = new DelegateCommand<Recipe>(OnEditTapped);
            searchCommand = new DelegateCommand(OnSearch);

            RefreshRecipeList();
        }


        private void OnSearch()
        {
                var temp = _recipe.Where(c => c.RecipeName.Contains(Bar));
                 Debug.WriteLine($"*** {this.GetType().Name}.{nameof(OnSearch)} temp: {temp}");
                Recipes = new ObservableCollection<Recipe>(temp);
        }

        private void OnEditTapped(Recipe ToEdit)
        {
            Debug.WriteLine($"*** {this.GetType().Name}.{nameof(OnEditTapped)}: ToEdit ID: {ToEdit.Id}");

            NavigationParameters navParams = new NavigationParameters();
            string pagetitle = "Edit Recipe";
            navParams.Add("RecipeTopicKey", ToEdit);
            navParams.Add("blockEdit", false);
            navParams.Add("visibileButton", true);
            navParams.Add("customPageTitling", pagetitle);

            _navigationService.NavigateAsync(nameof(addRecipePage), navParams, false, true);   
        }

        private async void OnDeleteTapped(Recipe ToDelete)
        {
            Debug.WriteLine($"*** {this.GetType().Name}.{nameof(OnDeleteTapped)}: OnDelete");

            bool response = await _pageDialogService.DisplayAlertAsync("Remove Recipe","Are you sure you want to delete this recipe?", "No", "Yes");//Must connect to responce
            if(response == false)
            {
                _repository.DeleteRecipeTopic(ToDelete);
                 await RefreshRecipeList();
            }
        }


        private async void OnNavToAdd()
        {
            Debug.WriteLine($"*** {this.GetType().Name}.{nameof(OnNavToAdd)}: OnAddPage");

            // add true in the middle so you return after save  & cancel instead of backbutton
            NavigationParameters navParams = new NavigationParameters();
            string pagetitle = "Add Recipe";
            navParams.Add("visibileButton", true);
            navParams.Add("customPageTitling", pagetitle);

            await _navigationService.NavigateAsync(nameof(addRecipePage), navParams, false, true);
        }

        private async Task RefreshRecipeList()
        {
            ShowIsBusySpinner = true;
            SelectedRecipe = null;
            Recipes = await _repository.GetRecipeTopics();
            ShowIsBusySpinner = false;
        }
        private async void OnPullToRefresh()
        {
            await RefreshRecipeList();
        }

        private void OnRecipeTapped(Recipe recipeTapped)
        {
            Debug.WriteLine($"*** {this.GetType().Name}.{nameof(OnRecipeTapped)}: OnRecipeTapped ID{recipeTapped.Id}");
            NavigationParameters navParams = new NavigationParameters();
            navParams.Add("RecipeTopicKey", recipeTapped);
            navParams.Add("blockEdit", true);// will clock edits
            navParams.Add("visibileButton", false);// make buttons invisible


            _navigationService.NavigateAsync(nameof(addRecipePage), navParams, false, true);
        }
        private void OnRecipeSelected(Recipe recipeSelected)
        {
            Debug.WriteLine($"*** {this.GetType().Name}.{nameof(OnRecipeTapped)}: OnRecipeSelected");

        }

        // overrides the OnNavigatingto in VM base
        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnNavigatingTo)}: Recipe Update Override");

            base.OnNavigatingTo(parameters);
            OnPullToRefresh();
        }
      
    }
}