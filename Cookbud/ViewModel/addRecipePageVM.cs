//AddRecipe Page view model
using Xamarin.Forms;
using Cookbud.Models;
using Cookbud.Services;
using Cookbud.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using Plugin.Connectivity;
using Prism.Services;
using System.Net;



namespace Cookbud.ViewModel
{
    public class addRecipePageVM : ViewModelBase
    {
        IRepository _repository;// gives access to repository
        IPageDialogService _pageDialogService;

        public DelegateCommand SaveCommand { get; set; }//xaml page 
        public DelegateCommand CancelCommand { get; set; }//xaml page
        public DelegateCommand webPageCommand { get; set; }
        //handles the edit option, when user click on edit on the list view user will be able to edit content.
        private bool _editOption;
        public bool EditOption
        {
            get { return _editOption; }
            set { SetProperty(ref _editOption, value); }
        }

        // handles buttons visibility, when user wants to view recipe they will not be able to edit any content.
        private bool _buttonVisibility;
        public bool ButtonVisibility
        {
            get { return _buttonVisibility; }
            set { SetProperty(ref _buttonVisibility, value); }
        }


        private Recipe _thisRecipeTopic;
        public Recipe ThisRecipeTopic
        {
            get { return _thisRecipeTopic; }
            set { SetProperty(ref _thisRecipeTopic, value); }
        }

        private string _recipeTopicIdText;
        public string RecipeTopicIdText
        {
            get { return _recipeTopicIdText; }
            set { SetProperty(ref _recipeTopicIdText, value); }
        }

        private bool _showIsBusySpinner;
        public bool ShowIsBusySpinner
        {
            get { return _showIsBusySpinner; }
            set { SetProperty(ref _showIsBusySpinner, value); }
        }

        public addRecipePageVM(INavigationService navigationService, IRepository reopsitory, IPageDialogService pageDialogService) : base(navigationService)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(addRecipePageVM)}:  constructor");
            _repository = reopsitory;
            _pageDialogService = pageDialogService;

            SaveCommand = new DelegateCommand(OnSave);// deals with when save is active
            CancelCommand = new DelegateCommand(OnCancel);// active when Cancel Command is active
            webPageCommand = new DelegateCommand(NavToWebPage);
        }

        private void NavToWebPage()
        {
            var net = ThisRecipeTopic.webUrl;
            if (!net.Contains("https://"))
            {
                net = "https://" + net;
            }
            var address = new Uri(net);
            Device.OpenUri(address);        
        }

        // Overrides OnNavigatingTo from View Model
        public override void OnNavigatingTo(NavigationParameters parameters)
        {
            base.OnNavigatingTo(parameters);
            if (parameters.ContainsKey("customPageTitling"))// customize page title 
            {
                Title = (string)parameters["customPageTitling"];
            }
            if (parameters.ContainsKey("visibileButton"))
            {
                ButtonVisibility = (bool)parameters["visibileButton"];

                if (ButtonVisibility == false)// checks for connectivity 
                {
                    var isConnected = CrossConnectivity.Current.IsConnected;
                    if (isConnected == false)
                    {
                        _pageDialogService.DisplayAlertAsync("Alert", "You are not connected to the Internet, you will not be able to navigate to webpage", "Ok");
                    }

                }
            }
            if (parameters.ContainsKey("blockEdit"))
            {
                EditOption = (bool)parameters["blockEdit"];
            }

            if (parameters.ContainsKey("RecipeTopicKey"))
            {
                ThisRecipeTopic = parameters["RecipeTopicKey"] as Recipe;
            }
            else
            {
                ThisRecipeTopic = new Recipe();
            }
            RecipeTopicIdText = $"Recipe Topic Id: {ThisRecipeTopic.Id}";
        }

        // when Cancel is pressed it will navigate to prevous page 
        private async void OnCancel()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnCancel)}: Cancel");
            await _navigationService.GoBackAsync();
        }

        // when save is pressed it will save the entries to the list view and will navigate to the previous page
        private async void OnSave()
        {

            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnSave)}: Save");

            if (ThisRecipeTopic == null)// in case if entry was empty it will navigate to the Recipe list but It will not add it the the List View
            {
                await _navigationService.GoBackAsync();
            }
            else
            {
                ThisRecipeTopic.Id = await _repository.SaveRecipeTopic(ThisRecipeTopic);
                await Task.Delay(1);
                await _navigationService.GoBackAsync();
            }
        }

    }
}
