using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Cookbud.Views;
using Xamarin.Forms;
using Cookbud.Services;

namespace Cookbud.ViewModel
{

    public class CookBudPageVM : ViewModelBase
    {
        public DelegateCommand OnNavToShowRecipesCommand { get; set; }
        public DelegateCommand OnNavToShowAboutCommand { get; set; }
        public DelegateCommand OnNavToShowAddCommand { get; set; }

        public CookBudPageVM(INavigationService navigationService, IRepository reopsitory):base(navigationService)
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(CookBudPageVM)}:  ctor");

            OnNavToShowRecipesCommand = new DelegateCommand(NavToRecipesDisplayPage);
            OnNavToShowAboutCommand = new DelegateCommand(NavToShowAboutPage);
            OnNavToShowAddCommand = new DelegateCommand(NavToShowAddPage);
        }

        private void NavToRecipesDisplayPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(NavToRecipesDisplayPage)}:  Nav To Recipe Page");
            _navigationService.NavigateAsync(nameof(RecipesPage), null, false, true);

        }

        private void NavToShowAboutPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(NavToShowAboutPage)}:  Nav To about Page");
            _navigationService.NavigateAsync(nameof(aboutPage), null, false, true);
        }

        private async void NavToShowAddPage()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(NavToShowAddPage)}:  Nav To add Page");
            NavigationParameters navParams = new NavigationParameters();
            string pagetitle = "Add Recipe";
            navParams.Add("visibileButton", true);
            navParams.Add("customPageTitling", pagetitle);

            await _navigationService.NavigateAsync(nameof(addRecipePage), navParams, false, true); 
        }
    }
}
 


