﻿using System;
using System.Collections.Specialized;
using System.Text;



namespace Cookbud.Helpers
{
    public static class CustomExtentionMethods
    {
        public static string PrettyPrint(this NotifyCollectionChangedEventArgs args)
        {
            var prettyStringBuilder = new StringBuilder("\n");
            prettyStringBuilder.AppendLine($"NewStartingIndex:  {args.NewStartingIndex}");
            prettyStringBuilder.AppendLine($"OldStartingIndex:  {args.OldStartingIndex}");

            if (args.NewItems != null)
            {
                prettyStringBuilder.AppendLine($"{args.NewItems?.Count} new items:");
                foreach (var item in args.NewItems)
                {
                    prettyStringBuilder.AppendLine($"\t{item.ToString()}");
                }
            }
            else
            {
                prettyStringBuilder.AppendLine($"0 new items.");
            }


            if (args.OldItems != null)
            {
                prettyStringBuilder.AppendLine($"{args.OldItems?.Count} old items:");
                foreach (var item in args.OldItems)
                {
                    prettyStringBuilder.AppendLine($"\t{item.ToString()}");
                }
            }
            else
            {
                prettyStringBuilder.AppendLine($"0 old items.");
            }

            prettyStringBuilder.AppendLine();

            return prettyStringBuilder.ToString();
        }
    }
}
