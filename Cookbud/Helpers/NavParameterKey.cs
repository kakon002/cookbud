﻿using System;

namespace Cookbud.Helpers
{
    public static class NavParameterKey
    {
        public const string ADD_NEW_RECIPE_KEY = "addNewRecipeKey";
    }
}
