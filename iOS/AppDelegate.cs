﻿using Cookbud.DependencyServices;
using Cookbud.iOS.DependencyServices;
using Foundation;
using Prism;
using Prism.Ioc;
using UIKit;

namespace Cookbud.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();

            LoadApplication(new App(new IOSInitializer()));

            return base.FinishedLaunching(app, options);
        }
    }
    public class IOSInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IContainerRegistry container)
        {
            // added new
            container.RegisterSingleton<IFileAccessService, FileAccessServiceIos>();
        }
    }
}
