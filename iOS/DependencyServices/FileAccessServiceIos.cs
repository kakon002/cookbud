﻿using System;
using System.IO;
using Cookbud.DependencyServices;
using Cookbud.iOS.DependencyServices;
using Xamarin.Forms;

[assembly: Dependency(typeof(FileAccessServiceIos))]
namespace Cookbud.iOS.DependencyServices
{
    public class FileAccessServiceIos: IFileAccessService
    {
  

        public string GetSqLiteDatabasePath(string databaseName)
        {
            string personalFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libraryFolder = Path.Combine(personalFolderPath, "..", "Library");
            if(!Directory.Exists(libraryFolder))
            {
                Directory.CreateDirectory(libraryFolder);
            }
            var dbPath = Path.Combine(libraryFolder, databaseName);
            Console.WriteLine($"*** {this.GetType().Name}.{nameof(GetSqLiteDatabasePath)}: returning:[{dbPath}]");
            return dbPath;
        
        }
    }
}
